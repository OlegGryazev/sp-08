<#import "../parts/common.ftl" as c>
<@c.page>
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th>Task ID</th>
            <th>Linked project</th>
            <th>Task name</th>
            <th>Details</th>
            <th>VIEW</th>
            <th>EDIT</th>
            <th>REMOVE</th>
        </tr>
        </thead>
        <tbody>
        <#list tasks as task>
            <tr>
                <td class="align-middle">${task.getId()}</td>
                <td class="align-middle">${task.getProjectId()!}</td>
                <td class="align-middle">${task.getName()!}</td>
                <td class="align-middle">${task.getDetails()!}</td>
                <td class="align-middle">
                    <form method="get" action="view-task">
                        <input type="hidden" name="taskId" value="${task.getId()}" />
                        <button class="btn btn-link" type="submit">VIEW</button>
                    </form>
                </td>
                <td class="align-middle">
                    <form method="get" action="edit-task">
                        <input type="hidden" name="taskId" value="${task.getId()}" />
                        <button class="btn btn-link" type="submit">EDIT</button>
                    </form>
                </td>
                <td class="align-middle">
                    <form method="post" action="task-remove">
                        <input type="hidden" name="taskId" value="${task.getId()}" />
                        <button class="btn btn-link" type="submit">REMOVE</button>
                    </form>
                </td>
            </tr>
        </#list>
        </tbody>
    </table>
    <a href="add-task" class="btn btn-primary">Add task</a>
    <a href="tasks" class="btn btn-secondary">Refresh</a>
</@c.page>