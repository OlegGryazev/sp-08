<#import "../parts/common.ftl" as c>
<@c.page>
    <div class="card card-body col-5">
        <form method="post" action="add-task">
            <div class="form-group">
                <label>Linked project</label>
                <select class="form-control" name="linkedProjectId" id="linkedProjectId">
                    <#list projects as project>
                        <option value="${project.getId()}">${project.getName()}</option>
                    </#list>
                </select>
            </div>
            <div class="form-group">
                <label>Task name</label>
                <input class="form-control mb-2" type="text" name="name" placeholder="Task name" />
            </div>
            <div class="form-group">
                <label>Task details</label>
                <input class="form-control mb-2" type="text" name="details" placeholder="Task details" />
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label>Date of task start</label>
                    <input class="form-control mb-2" type="date" name="dateStart" placeholder="Start" />
                </div>
                <div class="form-group col">
                    <label>Date of task finish</label>
                    <input class="form-control mb-2" type="date" name="dateFinish" placeholder="Finish" />
                </div>
            </div>
            <div class="form-group">
                <label>Task status</label>
                <select class="form-control" name="status" id="status">
                    <#list statuses as status>
                        <option value="${status}">${status.displayName()}</option>
                    </#list>
                </select>
            </div>
            <div class="form-row mt-3">
                <button class="btn btn-primary mr-4" type="submit">Add task</button>
                <a href="tasks" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </div>
</@c.page>