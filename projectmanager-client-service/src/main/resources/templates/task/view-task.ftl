<#import "../parts/common.ftl" as c>
<@c.page>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">${task.getName()}</h5>
            <p class="card-text">Task linked to project: ${task.getProjectId()}</p>
            <p class="card-text">Details about task: ${task.getDetails()}</p>
            <#if dateStart??>
                <p class="card-text">Task starts: ${dateStart}</p>
            </#if>
            <#if dateFinish??>
                <p class="card-text">Task finish: ${dateFinish}</p>
            </#if>
            <p class="card-text">Status of task: ${task.getStatus().displayName()}</p>
            <a href="tasks" class="btn btn-primary mt-3">Go back</a>
        </div>
    </div>
</@c.page>