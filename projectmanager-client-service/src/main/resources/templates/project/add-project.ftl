<#import "../parts/common.ftl" as c>
<@c.page>
    <div class="card card-body col-5">
        <form method="post" action="add-project">
            <div class="form-group">
                <label>Project name</label>
                <input class="form-control mb-2" type="text" name="name" placeholder="Project name" />
            </div>
            <div class="form-group">
                <label>Project details</label>
                <input class="form-control mb-2" type="text" name="details" placeholder="Project details" />
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label>Date of project start</label>
                    <input class="form-control mb-2" type="date" name="dateStart" placeholder="Start" />
                </div>
                <div class="form-group col">
                    <label>Date of project finish</label>
                    <input class="form-control mb-2" type="date" name="dateFinish" placeholder="Finish" />
                </div>
            </div>
            <div class="form-group">
                <label>Project status</label>
                <select class="form-control" name="status" id="status">
                    <#list statuses as status>
                        <option value="${status}">${status.displayName()}</option>
                    </#list>
                </select>
            </div>
            <div class="form-row mt-3">
                <button class="btn btn-primary mr-4" type="submit">Add project</button>
                <a href="projects" class="btn btn-secondary">Cancel</a>
            </div>
        </form>

    </div>
</@c.page>