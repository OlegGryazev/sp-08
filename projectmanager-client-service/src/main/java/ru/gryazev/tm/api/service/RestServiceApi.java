package ru.gryazev.tm.api.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.dto.User;

import java.util.List;
import java.util.Map;

@RequestMapping("/rest/users/")
@FeignClient(name = "projectmanager-service")
public interface RestServiceApi {

    @GetMapping(value="{userId}/projects", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<Project> getProjects(@PathVariable("userId") final String userId);

    @GetMapping(value="{userId}/projects/{projectId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Project getProject(
            @PathVariable("userId") final String userId,
            @PathVariable("projectId") final String projectId
    );

    @GetMapping(value="{userId}/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
    List<Task> getTasks(@PathVariable("userId") final String userId);

    @GetMapping(value="{userId}/tasks/{taskId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    Task getTask(
            @PathVariable("userId") final String userId,
            @PathVariable("taskId") final String taskId
    );

    @PostMapping(value="{userId}/projects/merge", headers = MediaType.APPLICATION_JSON_VALUE)
    Project mergeProject(
            @PathVariable("userId") final String userId,
            @RequestBody final Project project
    );

    @DeleteMapping(value="{userId}/projects/delete/{projectId}")
    void deleteProject(
            @PathVariable("userId") final String userId,
            @PathVariable("projectId") final String projectId
    );

    @PostMapping(value="{userId}/tasks/merge", headers = MediaType.APPLICATION_JSON_VALUE)
    Task mergeTask(
            @PathVariable("userId") final String userId,
            @RequestBody final Task task
    );

    @DeleteMapping(value="{userId}/tasks/delete/{taskId}")
    void deleteTask(
            @PathVariable("userId") final String userId,
            @PathVariable("taskId") final String taskId
    );

    @GetMapping(value = "{username}", headers = MediaType.APPLICATION_JSON_VALUE)
    User findUserByName(@PathVariable("username") final String username);

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    void createUser(@RequestBody final Map<String, String> credentials);

}
