package ru.gryazev.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER,
    MODERATOR

}
