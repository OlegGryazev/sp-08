package ru.gryazev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.RestServiceApi;
import ru.gryazev.tm.dto.User;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final RestServiceApi restServiceApi;

    @GetMapping("/login")
    public String login(){
        if(SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)
        ) {
            return "project/projects";
        }
        return "login";
    }

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password,
            Map<String, Object> model
    ){
        try {
            restServiceApi.findUserByName(username);
        } catch (Exception e) {
            final Map<String, String> credentials = new HashMap<>();
            credentials.put("username", username);
            credentials.put("password", password);
            restServiceApi.createUser(credentials);
            return "redirect:/login";
        }
        model.put("message", "User already exists!");
        return "registration";
    }

}
