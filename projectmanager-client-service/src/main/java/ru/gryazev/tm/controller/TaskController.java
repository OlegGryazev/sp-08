package ru.gryazev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.RestServiceApi;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final RestServiceApi restServiceApi;

    @GetMapping("tasks")
    public String getProjects(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final List<Task> tasks = restServiceApi.getTasks(user.getId());
        model.put("tasks", tasks);
        return "task/tasks";
    }

    @PostMapping({"add-task", "edit-task"})
    public String addTask(
            @RequestParam("linkedProjectId") final String projectId,
            @RequestParam(value = "taskId", required = false) final String taskId,
            @RequestParam("name") final String name,
            @RequestParam("details") final String details,
            @RequestParam("dateStart") final String dateStart,
            @RequestParam("dateFinish") final String dateFinish,
            @RequestParam("status") final String status,
            final Principal principal
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Task task = new Task();
        if (taskId != null) {
            task.setId(taskId);
        }
        task.setProjectId(projectId);
        task.setUserId(user.getId());
        task.setName(name);
        task.setDetails(details);
        task.setDateStart(DateUtils.formatStringToDate(dateStart));
        task.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        task.setStatus(Status.valueOf(status));
        restServiceApi.mergeTask(user.getId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("add-task")
    public String addTask(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final List<Project> projects = restServiceApi.getProjects(user.getId());
        model.put("projects", projects);
        model.put("statuses", Status.values());
        return "task/add-task";
    }

    @GetMapping("edit-task")
    public String editProject(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Task task = restServiceApi.getTask(user.getId(), taskId);
        @NotNull final List<Project> projects = restServiceApi.getProjects(user.getId());
        model.put("projects", projects);
        model.putAll(getModelByTask(task));
        return "task/edit-task";
    }

    @PostMapping("task-remove")
    public String removeTask(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        restServiceApi.deleteTask(user.getId(), taskId);
        return "redirect:/tasks";
    }

    @GetMapping("view-task")
    public String viewTask(
            @RequestParam("taskId") final String taskId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Task task = restServiceApi.getTask(user.getId(), taskId);
        model.putAll(getModelByTask(task));
        return "task/view-task";
    }

    private Map<String, Object> getModelByTask(final Task task) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(task.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(task.getDateFinish());
        model.put("task", task);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
