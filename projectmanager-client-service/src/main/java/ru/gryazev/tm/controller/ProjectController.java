package ru.gryazev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gryazev.tm.api.service.RestServiceApi;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class ProjectController {

    private final RestServiceApi restServiceApi;

    @GetMapping("projects")
    public String getProjects(
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final List<Project> projects = restServiceApi.getProjects(user.getId());
        model.put("projects", projects);
        return "project/projects";
    }

    @GetMapping("add-project")
    public String addProject(final Map<String, Object> model) {
        model.put("statuses", Status.values());
        return "project/add-project";
    }

    @PostMapping("project-remove")
    public String removeProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal
    ) {
        @NotNull User userEntity = restServiceApi.findUserByName(principal.getName());
        restServiceApi.deleteProject(userEntity.getId(), projectId);
        return "redirect:/projects";
    }

    @PostMapping({"add-project", "edit-project"})
    public String addProject(
            @RequestParam(value = "projectId", required = false) final String projectId,
            @RequestParam("name") final String name,
            @RequestParam("details") final String details,
            @RequestParam("dateStart") final String dateStart,
            @RequestParam("dateFinish") final String dateFinish,
            @RequestParam("status") final String status,
            final Principal principal
            ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Project project = new Project();
        if (projectId != null) project.setId(projectId);
        project.setUserId(user.getId());
        project.setName(name);
        project.setDetails(details);
        project.setDateStart(DateUtils.formatStringToDate(dateStart));
        project.setDateFinish(DateUtils.formatStringToDate(dateFinish));
        project.setStatus(Status.valueOf(status));
        restServiceApi.mergeProject(user.getId(), project);
        return "redirect:/projects";
    }

    @GetMapping("edit-project")
    public String editProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull final User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Project project = restServiceApi.getProject(user.getId(), projectId);
        model.putAll(getModelByProject(project));
        return "project/edit-project";
    }

    @GetMapping("view-project")
    public String viewProject(
            @RequestParam("projectId") final String projectId,
            final Principal principal,
            final Map<String, Object> model
    ) {
        @NotNull User user = restServiceApi.findUserByName(principal.getName());
        @NotNull final Project project = restServiceApi.getProject(user.getId(), projectId);
        model.putAll(getModelByProject(project));
        return "project/view-project";
    }

    @NotNull
    private Map<String, Object> getModelByProject(@NotNull final Project project) {
        Map<String, Object> model = new HashMap<>();
        @Nullable final String dateStart = DateUtils.formatDateToString(project.getDateStart());
        @Nullable final String dateFinish = DateUtils.formatDateToString(project.getDateFinish());
        model.put("project", project);
        model.put("dateStart", dateStart);
        model.put("dateFinish", dateFinish);
        model.put("statuses", Status.values());
        return model;
    }

}
