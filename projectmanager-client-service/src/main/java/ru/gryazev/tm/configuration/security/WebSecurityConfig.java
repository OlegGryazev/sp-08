package ru.gryazev.tm.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceBean userDetailsServiceBean;

    @Autowired
    public WebSecurityConfig(final UserDetailsServiceBean userDetailsServiceBean) {
        this.userDetailsServiceBean = userDetailsServiceBean;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/", "/login", "/registration").permitAll()
                .antMatchers("/rest/projects/**", "/rest/tasks/**").hasRole("USER")
                .antMatchers(HttpMethod.GET, "/rest/users/").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/rest/users/registration").permitAll()
                .antMatchers(HttpMethod.DELETE, "/rest/users/delete/self").hasRole("USER")
                .antMatchers(HttpMethod.POST, "/rest/users/**").hasRole("ADMINISTRATOR")
                .antMatchers(HttpMethod.DELETE, "/rest/users/**").hasRole("ADMINISTRATOR")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceBean)
                .passwordEncoder(passwordEncoder());
    }

}
