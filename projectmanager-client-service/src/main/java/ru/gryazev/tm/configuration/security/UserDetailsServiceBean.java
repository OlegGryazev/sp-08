package ru.gryazev.tm.configuration.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.gryazev.tm.api.service.RestServiceApi;
import ru.gryazev.tm.enumerated.RoleType;

@RequiredArgsConstructor
@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    private final RestServiceApi restServiceApi;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final ru.gryazev.tm.dto.User user = restServiceApi.findUserByName(username);
        if (user == null) throw new UsernameNotFoundException("User not found!");
        User.UserBuilder builder = User.withUsername(username);
        builder.password(user.getPassword());
        builder.roles(RoleType.USER.toString());
        return builder.build();
    }

}
