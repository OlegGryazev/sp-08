package ru.gryazev.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskServiceUT {

    @Mock
    private ITaskRepository taskRepository;

    @InjectMocks
    private TaskService taskService;

    private TaskEntity taskEntity;

    @Before
    public void initMock() {
        taskEntity = new TaskEntity();
        taskEntity.setUser(new UserEntity());
        List<TaskEntity> taskEntities = new ArrayList<>();
        taskEntities.add(taskEntity);
        taskEntity.setName("name");
        Mockito.when(taskRepository.findByIdAndUserId("id", "id")).thenReturn(Optional.of(taskEntity));
        Mockito.when(taskRepository.findByUserId("id")).thenReturn(taskEntities);
        Mockito.when(taskRepository.findByProjectId("id")).thenReturn(taskEntities);
        Mockito.when(taskRepository.save(taskEntity)).thenReturn(taskEntity);
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskFindByIdAndUserIdAllNullNegative() {
        taskService.findByIdAndUserId(null, null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskFindByIdAndUserIdAllEmptyNegative() {
        taskService.findByIdAndUserId("", "");
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskFindByIdAndUserIdUnknownNegative() {
        taskService.findByIdAndUserId("not_found", "not_found");
    }

    @Test
    public void taskFindByIdAndUserIdPositive() {
        assertEquals("name", taskService.findByIdAndUserId("id", "id").getName());
    }

    @Test
    public void taskFindByUserIdNegative() {
        assertEquals(0, taskService.findByUserId(null).size());
        assertEquals(0, taskService.findByUserId("not_found").size());
        assertEquals(0, taskService.findByUserId("").size());
    }

    @Test
    public void taskFindByUserIdPositive() {
        assertEquals(1, taskService.findByUserId("id").size());
    }

    @Test
    public void taskFindByProjectIdNegative() {
        assertEquals(0, taskService.findByProjectId(null).size());
        assertEquals(0, taskService.findByProjectId("not_found").size());
        assertEquals(0, taskService.findByProjectId("").size());
    }

    @Test
    public void taskFindByProjectIdPositive() {
        assertEquals(1, taskService.findByProjectId("id").size());
    }

    @Test
    public void taskSavePositive() {
        assertNotNull(taskService.save(taskEntity));
    }

    @Test
    public void taskSaveNegative() {
        assertNull(taskService.save(null));
        assertNull(taskService.save(new TaskEntity()));
    }

    @Test
    public void taskDeleteByIdAndUserIdPositive() {
        taskService.deleteByIdAndUserId("id", "id");
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskDeleteByIdAndUserIdAllNullNegative() {
        taskService.deleteByIdAndUserId(null, null);
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskDeleteByIdAndUserIdAllEmptyNegative() {
        taskService.deleteByIdAndUserId("", "");
    }

    @Test(expected = CrudNotFoundException.class)
    public void taskDeleteByIdAndUserIdUnknownNegative() {
        taskService.deleteByIdAndUserId("not_found", "not_found");
    }

}
