package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProjectRESTController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IUserService userService;

    @Autowired
    public ProjectRESTController(
            final IProjectService projectService,
            final ITaskService taskService,
            final IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @RequestMapping(value = "rest/users/{userId}/projects", method = RequestMethod.GET)
    public List<Project> getProjects(@PathVariable("userId") final String userId) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        @NotNull final List<Project> projects = new ArrayList<>();
        projectService.findByUserId(userEntity.getId()).forEach(o -> projects.add(ProjectEntity.toProjectDto(o)));
        return projects;
    }

    @DeleteMapping("rest/users/{userId}/projects/delete/{projectId}")
    public void removeProject(
            @PathVariable("userId") final String userId,
            @PathVariable("projectId") final String projectId
    ) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        projectService.deleteByIdAndUserId(projectId, userEntity.getId());
    }

    @RequestMapping(value = "rest/users/{userId}/projects/merge", method = RequestMethod.POST)
    public void mergeProject(
            @PathVariable("userId") final String userId,
            @RequestBody final Project project
    ) {
        if (userId == null || project == null || !userId.equals(project.getUserId())) return;
        @Nullable ProjectEntity projectEntity = Project.toProjectEntity(project, taskService, userService);
        if (projectEntity == null) return;
        projectService.save(projectEntity);
    }

    @RequestMapping(value = "rest/users/{userId}/projects/{projectId}", method = RequestMethod.GET)
    public Project getProject(
            @PathVariable("userId") final String userId,
            @PathVariable("projectId") final String projectId
    ) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        @Nullable final ProjectEntity projectEntity = projectService.findByIdAndUserId(projectId, userEntity.getId());
        return ProjectEntity.toProjectDto(projectEntity);
    }

}
