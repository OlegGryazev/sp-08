package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.entity.UserEntity;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskRESTController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    private final IUserService userService;

    @Autowired
    public TaskRESTController(
            final ITaskService taskService,
            final IProjectService projectService,
            final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @RequestMapping(value = "rest/users/{userId}/tasks", method = RequestMethod.GET)
    public List<Task> getProjects(@PathVariable("userId") final String userId) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskService.findByUserId(userEntity.getId()).forEach(o -> tasks.add(TaskEntity.toTaskDto(o)));
        return tasks;
    }

    @RequestMapping(value = "rest/users/{userId}/tasks/merge", method = RequestMethod.POST)
    public void addTask(
            @PathVariable final String userId,
            @RequestBody final Task task
    ) {
        if (userId == null || task == null || !userId.equals(task.getUserId())) return;
        @Nullable final TaskEntity taskEntity = Task.toTaskEntity(task, projectService, userService);
        if (taskEntity == null) return;
        taskService.save(taskEntity);
    }

    @RequestMapping(value = "rest/users/{userId}/tasks/{taskId}", method = RequestMethod.GET)
    public Task getTask(
            @PathVariable("userId") final String userId,
            @PathVariable("taskId") final String taskId
    ) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        @Nullable final TaskEntity taskEntity = taskService.findByIdAndUserId(taskId, userEntity.getId());
        return TaskEntity.toTaskDto(taskEntity);
    }

    @DeleteMapping("rest/users/{userId}/tasks/delete/{taskId}")
    public void removeTask(
            @PathVariable("userId") final String userId,
            @PathVariable("taskId") final String taskId
    ) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        taskService.deleteByIdAndUserId(taskId, userEntity.getId());
    }

}
