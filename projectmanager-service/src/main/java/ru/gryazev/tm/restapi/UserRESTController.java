package ru.gryazev.tm.restapi;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;

import java.util.Map;

@RestController
public class UserRESTController {

    private final IUserService userService;

    @Autowired
    public UserRESTController(final IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "rest/users/registration", method = RequestMethod.POST)
    public void createUser(@RequestBody final Map<String, String> credentials) {
        final String username = credentials.get("username");
        final String password = credentials.get("password");
        try {
            userService.findByUserName(username);
        } catch (CrudNotFoundException e) {
            userService.createUser(username, password, RoleType.USER);
        }
    }

    @RequestMapping(value = "rest/users/{username}", method = RequestMethod.GET)
    public User findUserByName(@PathVariable("username") final String username) {
        @Nullable final UserEntity userEntity = userService.findByUserName(username);
        return UserEntity.toUserDto(userEntity);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "rest/users/{userId}/delete/self", method = RequestMethod.DELETE)
    public void deleteSelf(@PathVariable("userId") final String userId) {
        @Nullable final UserEntity userEntity = userService.findById(userId);
        userService.deleteById(userEntity.getId());
    }

}
