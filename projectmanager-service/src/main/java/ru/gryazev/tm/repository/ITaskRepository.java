package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface ITaskRepository extends CrudRepository<TaskEntity, String> {

    List<TaskEntity> findByProjectId(@NotNull final String projectId);

    @NotNull
    Optional<TaskEntity> findByIdAndUserId(@NotNull String id, @NotNull String userId);

    @NotNull
    List<TaskEntity> findByUserId(@NotNull String userId);

}
