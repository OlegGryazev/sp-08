package ru.gryazev.tm.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class User {

    private String id = UUID.randomUUID().toString();

    private String username;

    private String password;

}
