package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.Role;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.repository.IUserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements IUserService {

    private final IUserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            final IUserRepository userRepository,
            final PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    private void init() {
        if (userRepository.findByUsername("test") != null) return;
        createUser("test", "test", RoleType.USER);
    }

    @Transactional
    public UserEntity createUser(final String username, final String password, final RoleType... roleTypes) {
        if (username == null || username.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleTypes == null) return null;
        final String encryptedPassword = passwordEncoder.encode(password);
        final UserEntity userEntity = new UserEntity();
        userEntity.setUsername(username);
        userEntity.setPassword(encryptedPassword);

        final List<Role> roles = new ArrayList<>();
        for (final RoleType roleType : roleTypes) {
            final Role role = new Role();
            role.setUser(userEntity);
            role.setRole(roleType);
            roles.add(role);
        }
        userEntity.setRoles(roles);
        return userRepository.save(userEntity);
    }

    @NotNull
    @Override
    public UserEntity findByUserName(@Nullable final String username) {
        if (username == null || username.isEmpty()) throw new CrudNotFoundException();
        @Nullable final UserEntity userEntity = userRepository.findByUsername(username);
        if (userEntity == null) throw new CrudNotFoundException();
        return userEntity;
    }

    @NotNull
    @Override
    public UserEntity findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new CrudNotFoundException();
        @Nullable final UserEntity userEntity = userRepository.findById(id).orElse(null);
        if (userEntity == null) throw new CrudNotFoundException();
        return userEntity;
    }

    @Override
    public void deleteById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new CrudNotFoundException();
        if (!userRepository.existsById(id)) throw new CrudNotFoundException();
        userRepository.deleteById(id);
    }

}
