#### Project Manager

###### Remote repository: 
https://gitlab.com/OlegGryazev/sp-08
###### Software requirements:
* JDK 8
* Apache Maven 3.6.3
* PostgreSQL 12
    
###### Technology stack:
* Maven
* Spring MVC
* Spring Security
* Spring Data JPA
* Spring Boot
* Spring Cloud OpenFeign
* Spring Cloud Eureka
* Spring Cloud Config
* Zuul Proxy
* Hibernate
* Spring Testing
* REST-assured
    
###### Developer:
    Gryazev Oleg
    email: gryazev77@gmail.com
     
###### Build&Run Config-service:
    cd config-service
    mvn clean install
    
    java -jar target/config-service-1.8.jar
    
###### Build&Run Discovery-service:
    cd discovery-service
    mvn clean install
    
    java -jar target/discovery-service-1.8.jar
    
###### Build&Run Projectmanager-client-service:
    cd projectmanager-client-service
    mvn clean install
        
    java -jar target/projectmanager-client-service-1.8.jar
        
###### Build&Run Projectmanager-service:
    cd projectmanager-service
    mvn clean install
    
    java -jar target/projectmanager-service-1.8.jar
    
###### Build&Run Zuul-gateway:
    cd zuul-gateway
    mvn clean install
    
    java -jar target/zuul-gateway-1.8.jar